# Akagu font the premiere nsibidi font

> If you don't know what nsibidi is [read the articles](https://gitlab.com/pierre.label.sio/akagu-font-nsibidi/tree/master/resources) on this repo and do your own research

## How to edit the font?

### Download a font editing tool

We use [fontforge](http://fontforge.github.io/en-US/) a free open source software.

### Edit the font

Just do it, you can research google to learn how, if you use fontforge read the [docs](http://fontforge.github.io/en-US/documentation/).

### Publish your change

Fork the project or issue a merge request. If you choose to fork you can do your own thing.
Though if you wish to publish your change on this main repo issue a merge request.

### That's it

Let's make us africans use our own writings.