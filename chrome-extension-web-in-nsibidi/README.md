# Chrome extension to translate the web in nsibidi

![Screenshot using the extension it displays most web pages in nsibidi](https://gitlab.com/pierre.label.sio/akagu-font-nsibidi/raw/master/chrome-extension-web-in-nsibidi/Capture_d_%C3%A9cran_de_2019-10-23_19-32-53.png)
![Screenshot using the extension it displays most web pages in nsibidi](https://gitlab.com/pierre.label.sio/akagu-font-nsibidi/raw/master/chrome-extension-web-in-nsibidi/Capture_du_2019-10-26_21-08-36.png)

> It's only available in development mode yet but it can display most web pages in nsibidi and if you put your mouse over a text it gives you the english translation in latin character

