# Nsibidi Lexicon

## Pre requisite

Have the text editing tool Libre office free open source software
Have the akagu font installed on your computer ([Just download it](https://gitlab.com/pierre.label.sio/akagu-font-nsibidi/blob/master/Akagu.ttf) and click on it, will do on most OS)

### Edit the font

Just do it! 

### Publish your change

Fork the project or issue a merge request. If you choose to fork you can do your own thing.
Though if you wish to publish your change on this main repo issue a merge request.

### That's it

Let's make us africans use our own writings.
